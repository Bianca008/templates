#pragma once
#include<iostream>

template<typename T>
void f(T) {
	std::cout << "Prima functie f!\n";
}

template<>
void f(int x) {
	std::cout << "A doua functie f!\n";
}

void f(double x) {
	std::cout << "A treia functie f!\n";
}