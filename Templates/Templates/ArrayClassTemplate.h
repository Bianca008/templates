#pragma once
#include<iostream>
#include<algorithm>

template<class T>
class Array {
private:
	T* m_arr;
	int m_size;
public:
	Array();
	~Array();

	template<typename T>
	friend std::istream& operator>> (std::istream&, Array<T>&);
	template<typename T>
	friend std::ostream& operator<<(std::ostream&, Array<T>&);
};

template<typename T>
Array<T>::Array() {
	m_size = 0;
	m_arr = nullptr;
}

template<typename T>
Array<T>::~Array() {
	delete[] m_arr;
}

template<typename T>
std::istream& operator>>(std::istream& in, Array<T>& arr) {
	in >> arr.m_size;
	arr.m_arr = new T[arr.m_size];
	for (int i = 0; i < arr.m_size; i++)
		in >> arr.m_arr[i];
	return in;
}

template<typename T>
std::ostream& operator<<(std::ostream& out, Array<T>& arr) {
	out << "Dimensiunea array-ului etse: " << arr.m_size << "\n";
	out << "Elementele sunt: ";
	for (int i = 0; i < arr.m_size; i++)
		out << arr.m_arr[i] << " ";
	out << std::endl;
	return out;
}