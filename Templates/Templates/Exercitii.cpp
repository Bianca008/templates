#include "Templates.h"
#include "ArrayClassTemplate.h"

int main()
{
	auto x = 5;
	auto y = 5.5;
	auto z = static_cast<float>(5.5);
	f(x);
	f(y);
	f(z);

	Array<int> a;
	std::cin >> a;
	std::cout << a;

	system("pause");
	return 0;
}